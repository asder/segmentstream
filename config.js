module.exports = {
    CAPACITY: process.env.CAPACITY || 50000,
    KEY_EXPIRY: process.env.KEY_EXPIRY || 60000,
    PORT: process.env.PORT || 3000,
    REDIS_ADDRESS: process.env.REDIS_ADDRESS || '127.0.0.1:6379'
};