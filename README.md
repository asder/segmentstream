# redis-proxy


A simple redis proxy, providing a HTTP API and caching layer for redis GET commands.

## Usage

**Running:**

```bash
# clone the repo
git clone https://gitlab.com/asder/segmentstream.git

# cd into repo
cd segmentstream

# build and run tests
make test

# build container
make build

# run built container
make start

# stop container
make stop
```

Configuration variables can be passed threw environment variables:

```
CAPACITY
KEY_EXPIRY
PORT
REDIS_ADDRESS

docker run --env KEY_EXPIRY=[KEY_EXPIRY] --env PORT=[SERVER_PORT] -p [PUBLIC_PORT]:[SERVER_PORT] --expose=[SERVER_PORT] --name segstream -d segmentstream
```

**API:**

 `GET /get/:key` returns the value associated with key param. Returns from cache if available, otherwise retrieves from redis


## Architecture Overview

Each request to the server is put in a queue based on key value. 
This is done for the case of multiple requests for the same key while it wasn't cached. 
In other words to reduce requests to Redis. Only one request is reaching Redis, others get cached value.    


