FROM node:14.15.1
RUN apt update && apt -y install redis-server

RUN mkdir -p /usr/src/app/

COPY ./package*.json /usr/src/app/

RUN cd /usr/src/app && npm install --production

WORKDIR /usr/src/app

COPY . /usr/src/app

CMD [ "npm", "start" ]

