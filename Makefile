build:
	docker build -t segmentstream .

test:
	docker build -t segmentstream . && docker run -d --name segtest segmentstream && docker exec -it segtest npm run test && docker rm segtest --force

start:
	docker run --name segstream -d segmentstream

stop:
	docker rm segstream --force