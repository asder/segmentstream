const {CacheService} = require('./cacheService');

test('set() and get()', () => {
    const cacheService = new CacheService({capacity: 2, keyExpiry: 60});
    cacheService.set('key', 'value');
    expect(cacheService.get('key')).toBe('value');
});

test('has()', () => {
    const cacheService = new CacheService({capacity: 2, keyExpiry: 60});
    cacheService.set('key', 'value');
    expect(cacheService.has('key')).toBe(true);
});

test('capacity', () => {
    const cacheService = new CacheService({capacity: 2, keyExpiry: 60});
    cacheService.set('key1', 'value1');
    cacheService.set('key2', 'value2');
    cacheService.set('key3', 'value3');
    expect(cacheService.get('key3')).toBe('value3');
    expect(cacheService.get('key2')).toBe('value2');
    expect(cacheService.get('key1')).toBe(undefined);
});

test('keyExpiry', (done) => {
    const cacheService = new CacheService({capacity: 2, keyExpiry: 0});
    cacheService.set('key', 'value');
    setTimeout(() => {
        expect(cacheService.get('key')).toBe(undefined);
        done();
    }, 100);
});