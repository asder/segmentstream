const {RequestService} = require('./requestService');

const cacheServiceMock = {
    has() {},
    get() {},
    set() {}
};
const redisServiceMock = {
    get() {}
};
const requestService = new RequestService({cacheService: cacheServiceMock, redisService: redisServiceMock});

describe('get()', () => {
    describe('when cache has value', () => {
        beforeEach(() => {
            cacheServiceMock.has = () => true;
            cacheServiceMock.get = () => 'cachedValue'
        });
        test('to take from cache', async(done) => {
            const result = await requestService.get('someKey');
            expect(result).toBe('cachedValue');
            done();
        });
    });
    describe('when cache has no value', () => {
        beforeEach(() => {
            cacheServiceMock.has = () => false;
            redisServiceMock.get = () => Promise.resolve('redisValue');
        });
        test('to take from redis', async(done) => {
            const result = await requestService.get('someKey');
            expect(result).toBe('redisValue');
            done();
        });
    });
});