const {QueueService} = require('./queueService');

test('queueRequest()', (done) => {
    const testValue = 'testValue';
    const testKey = 'testKey';
    const requestServiceMock = {
        get() {
            return Promise.resolve(testValue);
        }
    };
    const queueService = new QueueService({requestService: requestServiceMock});
    const reqMock = {
        params: {
            key: testKey
        }
    };
    const mockSend = jest.fn(x => x);
    const resMock = {
        send: mockSend
    };
    queueService.queueRequest(reqMock, resMock);
    setTimeout(() => {
        expect(mockSend.mock.results[0].value).toEqual({value: testValue, key: testKey});
        done();
    }, 100);
});
