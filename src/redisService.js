class RedisService {
    #redis
    constructor({redis}) {
        this.#redis = redis;
    }

    async get(key) {
        const result = await this.#redis.get(key);
        return result;
    }
}

module.exports = {
    RedisService
};