class QueueService {
    #map = new Map();
    #requestService;

    constructor({requestService}) {
        this.#requestService = requestService;
    }

    queueRequest = async(req, res) => {
        const {key} = req.params;
        if(this.#map.has(key)) {
            const queue = this.#map.get(key);
            queue.push(res);
        } else {
            const queue = [res];
            this.#map.set(key, queue);
            const value = await this.#requestService.get(key);
            queue.forEach((res) => {
                res.send({key, value});
            });
            this.#map.delete(key);
        }
    }
}

module.exports = {
    QueueService
};