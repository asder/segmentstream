const {RedisService} = require('./redisService');

const redisMock = {
    get() {
        return Promise.resolve('value');
    }
};

test('get()', async(done) => {
    const redisService = new RedisService({redis: redisMock});
    const result = await redisService.get('mockKey');
    expect(result).toBe('value');
    done();
});