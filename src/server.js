const {CAPACITY, KEY_EXPIRY, PORT, REDIS_ADDRESS} = require('../config');
const express = require('express');
const Redis = require('ioredis');
const redis = new Redis(REDIS_ADDRESS);
const {CacheService} = require('./cacheService');
const {QueueService} = require('./queueService');
const {RedisService} = require('./redisService');
const {RequestService} = require('./requestService');

cacheService = new CacheService({capacity: CAPACITY, keyExpiry: KEY_EXPIRY});
redisService = new RedisService({redis});
requestService = new RequestService({cacheService, redisService});
queueService = new QueueService({requestService});

const startServer = () => {
    const server = express();

    server.get('/get/:key', queueService.queueRequest);

    return new Promise((resolve) => {
        server.listen(PORT, () => {
            resolve(server);
            console.log(`Server is listening on http://localhost:${PORT}`);
        });
    });
}

module.exports = {
    startServer
};