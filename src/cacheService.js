class CacheService {
    #capacity
    #keyExpiry
    #map = new Map()

    constructor({capacity, keyExpiry}) {
        this.#capacity = capacity;
        this.#keyExpiry = keyExpiry;
    }

    get(key) {
        return this.#map.get(key);
    }

    has(key) {
        return this.#map.has(key);
    }

    set(key, value) {
        if(this.#map.size === this.#capacity) {
            const firstKey = this.#map.keys().next().value;
            this.#map.delete(firstKey);
        }
        this.#map.set(key, value);

        setTimeout(() => {
            this.#map.delete(key);
        }, this.#keyExpiry);
    }
}

module.exports = {
    CacheService
};