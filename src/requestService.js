class RequestService {
    #cacheService
    #redisService
    constructor({cacheService, redisService}) {
        this.#cacheService = cacheService;
        this.#redisService = redisService;
    }

    async get(key) {
        if(this.#cacheService.has(key)) {
            return this.#cacheService.get(key);
        }
        const result = await this.#redisService.get(key);
        this.#cacheService.set(key, result);
        return result;
    }
}

module.exports = {
    RequestService
};